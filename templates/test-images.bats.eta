#!/usr/bin/env bats
<%~ includeFile('partials/generated-file-header') %>

source "$BATS_TEST_DIRNAME/lib/eventlog.sh"

setup_file() {
<%
it.fn.assert(it.buildType === "dev" || it.buildType === "release", `invalid buildType: ${it.buildType}`);

if (it.buildType === "dev") {
%>
  EVENT_LOG=".out/dev-image-build-events.json-seq"
  IMAGE_IDS_BY_REPOTAG="$( \
    eventlog_Read .out/dev-image-build-events.json-seq | \
    eventlog_EventsOfType "DevImagesBuilt" | \
    eventlog_Last | \
    eventlog_ImageIdsByRepoTag \
  )"
<%
} else {
%>
  EVENT_LOG=".out/dev-image-build-events.json-seq"
  RELEASE_TAG="<%~ it.releaseGitTag %>"
  IMAGE_IDS_BY_REPOTAG="$( \
    eventlog_Read | \
    eventlog_ReleaseImagesBuilt "${RELEASE_TAG:?}" | \
    eventlog_Last | \
    eventlog_ImageIdsByRepoTag \
  )"
<%
}
%>

  if [[ $IMAGE_IDS_BY_REPOTAG == "" ]]; then
    echo "Error: image build info not found in $EVENT_LOG" 1>&2
    exit 1
  fi
  export IMAGE_IDS_BY_REPOTAG
}

<%
const assertOutputContainsVersionNumber = String.raw`echo "$output" | grep -P '\b[a-z]?\d+(?:\.\d+){2}\b'`;
const slimContainerCommandTests = [
  {
    name: "docker command can be executed",
    command: "docker --version",
    assertion: assertOutputContainsVersionNumber,
  },
  {
    name: "docker compose can be executed via docker subcommand using 'docker compose'",
    command: "docker compose version",
    assertion: assertOutputContainsVersionNumber,
  },
  {
    name: "docker compose can be executed via docker-compose-switch using 'docker-compose'",
    command: "docker-compose version",
    assertion: assertOutputContainsVersionNumber,
  },
];
const fullContainerCommandTests = [
  ['bash', '--version', 'GNU bash'],
  ['git', '--version', 'git'],
  ['grep', '--version', 'GNU grep'],
  ['ssh', '-V', 'OpenSSH'],
  ['make', '--version', 'GNU make'],
  ['curl', '--version', 'curl'],
].map(([cmd, versionFlag, name]) => ({
  name: `${cmd} command can be executed`,
  command: `${cmd} ${versionFlag}`,
  assertion: `head -n 1 <<<"$output" | grep --fixed-strings --ignore-case "${name}"`,
}));


it.imageBuildSpecs.forEach(({ target, repoTags }) => {
  repoTags.forEach((repoTag) => {
%>

@test "<%~ repoTag %>: tag references built ImageId" {
  local REPO_TAG="<%~ repoTag %>"
  local EXPECTED_ID="$(<<<"${IMAGE_IDS_BY_REPOTAG:?}" eventlog_ImageIdByRepoTag "${REPO_TAG:?}")"
  run docker image inspect --format '{{.Id}}' "${REPO_TAG:?}"
  echo output: "$output expected: $EXPECTED_ID"
  [ "$status" -eq 0 ]
  [[ "$output" = "${EXPECTED_ID:?}" ]]
}

<%
    let runContainerCommandTests;
    if (target === 'slim') {
      runContainerCommandTests = slimContainerCommandTests;
    }
    else if (target === 'full') {
      runContainerCommandTests = [...slimContainerCommandTests, ...fullContainerCommandTests];
    }
    else {
      throw new Error(`No tests defined for image build target: ${target}`);
    }

    runContainerCommandTests.forEach(({ name, command, assertion }) => {
%>

@test "<%~ repoTag %>: <%~ name %>" {
  run docker container run --rm "<%~ repoTag %>" \
    <%~ command
%>

  echo output: "$output"
  [ "$status" -eq 0 ]
  <%~ assertion
%>

}
<%
    })
  })
})
%>
