# Changelog

All notable changes to this project will be documented in this file. See
[standard-version](https://github.com/conventional-changelog/standard-version)
for commit guidelines.

### [2.0.2](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/compare/v2.0.1...v2.0.2) (2021-12-15)

### Bug Fixes

- **docker:** use correct architecture for docker-compose executable
  ([245035b](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/245035bcb40d07f36e15c64199fe2252b9acb42c))

### [2.0.1](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/compare/v2.0.0...v2.0.1) (2021-12-14)

## [2.0.0](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/compare/v1.1.0...v2.0.0) (2021-12-10)

### ⚠ BREAKING CHANGES

- Support for Bitbucket Pipelines has been removed, compose is now v2 not v1

### Features

- use compose v2, latest docker and remove bitbucket support
  ([1dd7a7b](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/1dd7a7ba93b692a2b2b3e2ad05940e62c01e9d17))

## 1.1.0 (2021-12-03)

### ⚠ BREAKING CHANGES

- Default stuff that comes with Ubuntu will no longer be present. It shouldn't
  have much effect though, as most things will be done through a specific image
  selected by the project's docker-compose.yml

### Features

- add ssh to image
  ([51f0b9a](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/51f0b9a79c1d628661f3bbe082874eda7ef34f48))
- Create initial Docker image
  ([103fa7b](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/103fa7b5bf1bf245f12636a2c6890ad42408c110))
- Expose docker daemon to nested containers
  ([b833c56](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/b833c56b8ae85b5443d6f8da439e59de0f968f9e))
- Update docker-compose and docker-cli
  ([39c0c48](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/39c0c48b28928f400b1aa3b12f2d1ec948f59b85))
- update to latest alpine, docker-cli and docker-compose versions
  ([70fa94a](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/70fa94a257935fb8785a2cf1804b864e608078c9))
- Use alpine instead of ubuntu for base image
  ([a112953](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/a112953253d57b0032386373f4e1f750e838c0d2))

### Bug Fixes

- Prevent our docker binary being replaced
  ([b273026](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/b273026974d1406613fa6d4b9c0ca3a8d425c8c0))
- Remove unused docker-entrypoint.sh
  ([a02078e](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/a02078efadf04a955b52b3c1a86eb5cbd19e7cbb))
- Update incorrect labels in Dockerfile
  ([f5a6e88](https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/commit/f5a6e882705e704640eecb709b4bd7d93fef3951))
