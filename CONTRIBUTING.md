# Contributing

The `Makefile` is used to run the various project tasks for building, testing
and publishing images.

```console
$ make
Available targets:
  help
  clean
  clean-generated-files
  lint
  check-format
  auto-format-files
  render-templates
  build-dev-image
  test-dev-image
  build-release-image
  test-release-image
  push-release-image
  bump-version
```

An example of making and publishing a new image version is in
[docs/release_example.md](docs/release_example.md).

## Automation

> Note: This project is being used as a testbed for project automation
> functionality we need to use across projects. For example, automatic
> dependency updates and automatic publishing of new versions.

With this goal of automation in mind, this project has a few notable features
which need to be understood:

- A config file ([`config.json`](config.json)) records the version of the
  project and details of its dependencies (like a `package.json` or `pom.xml`)
- The majority of project files are auto-updated by rendering templates (in the
  [./templates/](templates) directory) with parameters from `config.json`.
- `$ make render-templates` does this. It must be run after updating versions in
  `config.json`
- Versions in `config.json` still need to be manually updated, but having them
  defined explicitly in a file like this makes it possible to automate the
  update process. (I (Hal) have started working on this.)
- [`project-events.json`](project-events.json) is an experimental way to make
  historical project information available to automation tasks. Currently it
  records details of every tagged release that occurs, capturing Docker image
  hashes along with their tag names.
  - When testing and pushing images, we can ensure that the exact image that was
    first released is the one being tested/pushed (rebuilding a Docker image can
    result in a different result, e.g. due to changes to upstream images,
    updates to packages installed during the build process, changes to the local
    build context, etc).
  - Knowing the full range of image versions allows pushing updates to shorthand
    tags like `:latest`, `:2.0`, or `:2` without incorrectly making the tag
    point to an older image. E.g. `:latest` should only be updated to the image
    that is actually the very latest build, and `:2` only the latest `2.x.x`
    image.
  - Verification for users that an image they have is exactly the one we built
    and released.
  - Yet-unknown future use cases.

### Project automation scripting

The template rendering and related automation scripting is coded in
Javascript/Typescript and runs with [Deno]. Deno is very well suited for this
kind of build/project scripting work. It's Javascript/Typescript which is
familiar to many. Its module import system works by referencing `https://` urls
which are fetched directly, so there's no need to manage dependencies for build
scripts, but you can still pull in modules to do in-depth things where needed.
And you get the usual benefits from Typescript, without any complicated
pre-compilation steps. Deno also bundles the important dev tools, like a code
formatter, linter, test runner and more, so it's pretty batteries-included, easy
to get started but scales up to complex programs.

[Deno]: https://deno.land/

To learn about the background of Deno,
[The Changelog – Episode #443 Exploring Deno Land 🦕](https://changelog.com/podcast/443)
is worth a listen.

## Commit messages

We use [`standard-version`] to automatically determine the semantic version
number of new releases, and generate CHANGELOG entries. This works by analysing
commit messages, and so commits must follow the [Conventional Commits] rules.

[`standard-version`]: https://github.com/conventional-changelog/standard-version
[Conventional Commits]: https://www.conventionalcommits.org/

## Roadmap

These items are more related to project automation goals than this image itself
(which is very simple; hence why it's a good testbed).

- [ ] Release first 2.x version
- [ ] Run project automation tasks with CI
- [ ] Implement automatic dependency version upgrades
- [ ] Use semantic-release instead of standard-version to do version updates and
  publish releases (it's more fully-featured).
- [ ] Automate project structure itself. It's all well and good automating the
  manual work of version updates and publishing, but if that automation
  functionality itself requires significant work per-project then it's still a
  big cost. Ideally we need a way to both generate project management files and
  keep them up-to-date. [Projen](https://github.com/projen/projen) is one tool
  that is taking this approach, but I'm not sure it's a winner. It works well if
  your needs fit with its solutions, but it's awkward to customise. I feel like
  Deno's low barrier to entry (especially its simple HTTP URL dependency system)
  could make it very well suited to tackling this problem.
