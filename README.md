# `docker-compose` pipelines

A Docker image to run [`docker-compose`][docker-compose] commands in the CI
environments, particularly GitLab.

## Description

The image has two variants: regular and `slim`. The slim image is just the
[official docker image] with Docker Compose v2 and `docker-compose-switch`
(which provides backwards compatibility with the old `docker-compose` command
invocation style). (Docker don't yet provide a Compose v2 image, as it's not the
official current version, despite being stable.)

[official docker image]: https://hub.docker.com/_/docker/

The regular variant is `slim` with extra packages commonly used in CI
situations: bash, git, grep, make and curl.

## Image Tags

Images are published to this repo's [container registry] as
`registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:*`,
see the registry page for the available tags.

[container registry]: https://gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines/container_registry/

Tags follow [Semantic Versioning] rules and don't correspond to docker/docker
compose versions.

See the [CHANGELOG](CHANGELOG.md) for tag version history.

[Semantic Versioning]: https://semver.org/

## Contributing / Development

See [CONTRIBUTING.md](CONTRIBUTING.md).
