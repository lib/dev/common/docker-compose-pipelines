# Release example

This is an example of creating and publishing a new version of the repo's
images, taken from the release of `v2.0.2`.

To start, the `build-release-image` make target is run to calculate the new
version number and re-generate project files with the new version:

```console
$ make bump-version
npx standard-version@^9.3.2 --commit-all
npx: installed 190 in 13.681s
✔ Running lifecycle script "prerelease"
ℹ - execute command: "./pre-version.sh"
✔ bumping version in config.json from 2.0.1 to 2.0.2
✔ outputting changes to CHANGELOG.md
✔ Running lifecycle script "postchangelog"
ℹ - execute command: "make render-templates auto-format-files && git add --all"
/[...]/CHANGELOG.md
Checked 12 files

✔ committing config.json and CHANGELOG.md and all staged files
✔ tagging release v2.0.2
ℹ Run `git push --follow-tags origin main` to publish
```

Next `build-release-image` builds the Docker images and appends a build event to
the JSON event log describing the images that were created:

```console
$ make build-release-image
[+] Building (13/13) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 2.00kB
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/docker:20.10.11
 => [internal] load metadata for docker.io/library/buildpack-deps:curl
 => [docker-compose-switch 1/3] FROM docker.io/library/buildpack-deps:curl
 => [slim 1/3] FROM docker.io/library/docker:20.10.11@sha256:ff4fcc30e7c20f33c021e82683aedd1fe66654363eb1ff61065e0628f5bbf107
 => CACHED [docker-compose 2/3] RUN curl --fail --location --output /docker-compose   "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64"
 => CACHED [docker-compose 3/3] RUN chmod +x /docker-compose
 => CACHED [slim 2/3] COPY --from=docker-compose /docker-compose /usr/local/lib/docker/cli-plugins/docker-compose
 => CACHED [docker-compose-switch 2/3] RUN curl --fail --location --output /docker-compose-switch   "https://github.com/docker/compose-switch/releases/download/v1.0.3/docker-compose-linux-amd64"
 => CACHED [docker-compose-switch 3/3] RUN chmod +x /docker-compose-switch
 => CACHED [slim 3/3] COPY --from=docker-compose-switch /docker-compose-switch /usr/local/bin/docker-compose
 => exporting to image
 => => exporting layers
 => => writing image sha256:753ac909bf06fdd7b99d6bf33e893f3bd7e07f24ece4fbfe9cf17d9adc4dd52d
 => => naming to registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2-slim
 => => naming to registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0-slim
 => => naming to registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2-slim
[+] Building (14/14) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 37B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/docker:20.10.11
 => [internal] load metadata for docker.io/library/buildpack-deps:curl
 => [docker-compose-switch 1/3] FROM docker.io/library/buildpack-deps:curl
 => [slim 1/3] FROM docker.io/library/docker:20.10.11@sha256:ff4fcc30e7c20f33c021e82683aedd1fe66654363eb1ff61065e0628f5bbf107
 => CACHED [docker-compose 2/3] RUN curl --fail --location --output /docker-compose   "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64"
 => CACHED [docker-compose 3/3] RUN chmod +x /docker-compose
 => CACHED [slim 2/3] COPY --from=docker-compose /docker-compose /usr/local/lib/docker/cli-plugins/docker-compose
 => CACHED [docker-compose-switch 2/3] RUN curl --fail --location --output /docker-compose-switch   "https://github.com/docker/compose-switch/releases/download/v1.0.3/docker-compose-linux-amd64"
 => CACHED [docker-compose-switch 3/3] RUN chmod +x /docker-compose-switch
 => CACHED [slim 3/3] COPY --from=docker-compose-switch /docker-compose-switch /usr/local/bin/docker-compose
 => CACHED [full 1/1] RUN apk add --no-cache   bash   git   grep   openssh-client   make   curl
 => exporting to image
 => => exporting layers
 => => writing image sha256:7f8b368c41605df7370dcd7a9e1484ed4b79378a2c5d57f0c005b6b7eb4e2938
 => => naming to registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2
 => => naming to registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0
 => => naming to registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2
build-release-image: appended events to project-events.json-seq:
{
  "event": "ReleaseImagesBuilt",
  "date": "2021-12-15 06:53:49+00:00",
  "releaseGitTag": "v2.0.2",
  "releaseGitCommit": "5c4882dc10eb6653d73ccc094228115daeec3e86",
  "images": [
    {
      "dockerfile": "Dockerfile",
      "target": "slim",
      "id": "sha256:753ac909bf06fdd7b99d6bf33e893f3bd7e07f24ece4fbfe9cf17d9adc4dd52d",
      "repoTags": [
        "registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2-slim",
        "registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0-slim",
        "registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2-slim"
      ]
    },
    {
      "dockerfile": "Dockerfile",
      "target": "full",
      "id": "sha256:7f8b368c41605df7370dcd7a9e1484ed4b79378a2c5d57f0c005b6b7eb4e2938",
      "repoTags": [
        "registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2",
        "registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0",
        "registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2"
      ]
    }
  ]
}
[main 34a94dc] chore(release): log event ReleaseImagesBuilt for 2.0.2
 1 file changed, 28 insertions(+)
```

The changes to the event log are automatically committed following the release
build. With the events committed to the log, we can push the images via
`push-release-image`. This also runs the tests against the images before
pushing:

> Note: you need to be logged in to our container registry before pushing

```console
$ make push-release-image
Testing release images before pushing to ensure tags point to expected hashes and images are functional...
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2-slim: tag references built ImageId
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2-slim: docker command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2-slim: docker compose can be executed via docker subcommand using 'docker compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2-slim: docker compose can be executed via docker-compose-switch using 'docker-compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0-slim: tag references built ImageId
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0-slim: docker command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0-slim: docker compose can be executed via docker subcommand using 'docker compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0-slim: docker compose can be executed via docker-compose-switch using 'docker-compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2-slim: tag references built ImageId
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2-slim: docker command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2-slim: docker compose can be executed via docker subcommand using 'docker compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2-slim: docker compose can be executed via docker-compose-switch using 'docker-compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: tag references built ImageId
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: docker command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: docker compose can be executed via docker subcommand using 'docker compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: docker compose can be executed via docker-compose-switch using 'docker-compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: bash command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: git command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: grep command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: ssh command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: make command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2: curl command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: tag references built ImageId
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: docker command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: docker compose can be executed via docker subcommand using 'docker compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: docker compose can be executed via docker-compose-switch using 'docker-compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: bash command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: git command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: grep command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: ssh command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: make command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0: curl command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: tag references built ImageId
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: docker command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: docker compose can be executed via docker subcommand using 'docker compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: docker compose can be executed via docker-compose-switch using 'docker-compose'
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: bash command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: git command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: grep command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: ssh command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: make command can be executed
 ✓ registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines:2.0.2: curl command can be executed

42 tests, 0 failures

Proceeding to push images...
The push refers to repository [registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines]
ee7a2632331e: Pushed
5ccddfc98d4f: Pushed
70d694542e8b: Layer already exists
5a4e6f27ecd7: Layer already exists
c5811ba8cbee: Layer already exists
74f2fec1a062: Layer already exists
698317193ef4: Layer already exists
3de006b0ad1b: Layer already exists
1a058d5342cc: Layer already exists
2-slim: digest: sha256:eeaa961571c6ea59ec65d1b4878b5fc2515ca92716f7b3b6831fa5015ead4237 size: 2202
The push refers to repository [registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines]
ee7a2632331e: Layer already exists
5ccddfc98d4f: Layer already exists
70d694542e8b: Layer already exists
5a4e6f27ecd7: Layer already exists
c5811ba8cbee: Layer already exists
74f2fec1a062: Layer already exists
698317193ef4: Layer already exists
3de006b0ad1b: Layer already exists
1a058d5342cc: Layer already exists
2.0-slim: digest: sha256:eeaa961571c6ea59ec65d1b4878b5fc2515ca92716f7b3b6831fa5015ead4237 size: 2202
The push refers to repository [registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines]
ee7a2632331e: Layer already exists
5ccddfc98d4f: Layer already exists
70d694542e8b: Layer already exists
5a4e6f27ecd7: Layer already exists
c5811ba8cbee: Layer already exists
74f2fec1a062: Layer already exists
698317193ef4: Layer already exists
3de006b0ad1b: Layer already exists
1a058d5342cc: Layer already exists
2.0.2-slim: digest: sha256:eeaa961571c6ea59ec65d1b4878b5fc2515ca92716f7b3b6831fa5015ead4237 size: 2202
The push refers to repository [registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines]
2a5e0ddfdaeb: Pushed
ee7a2632331e: Layer already exists
5ccddfc98d4f: Layer already exists
70d694542e8b: Layer already exists
5a4e6f27ecd7: Layer already exists
c5811ba8cbee: Layer already exists
74f2fec1a062: Layer already exists
698317193ef4: Layer already exists
3de006b0ad1b: Layer already exists
1a058d5342cc: Layer already exists
2: digest: sha256:87690681b7865e54ebbe0245b8977547fd3c68258bc4ae7463e326d83cfef434 size: 2413
The push refers to repository [registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines]
2a5e0ddfdaeb: Layer already exists
ee7a2632331e: Layer already exists
5ccddfc98d4f: Layer already exists
70d694542e8b: Layer already exists
5a4e6f27ecd7: Layer already exists
c5811ba8cbee: Layer already exists
74f2fec1a062: Layer already exists
698317193ef4: Layer already exists
3de006b0ad1b: Layer already exists
1a058d5342cc: Layer already exists
2.0: digest: sha256:87690681b7865e54ebbe0245b8977547fd3c68258bc4ae7463e326d83cfef434 size: 2413
The push refers to repository [registry.gitlab.developers.cam.ac.uk/lib/dev/common/docker-compose-pipelines]
2a5e0ddfdaeb: Layer already exists
ee7a2632331e: Layer already exists
5ccddfc98d4f: Layer already exists
70d694542e8b: Layer already exists
5a4e6f27ecd7: Layer already exists
c5811ba8cbee: Layer already exists
74f2fec1a062: Layer already exists
698317193ef4: Layer already exists
3de006b0ad1b: Layer already exists
1a058d5342cc: Layer already exists
2.0.2: digest: sha256:87690681b7865e54ebbe0245b8977547fd3c68258bc4ae7463e326d83cfef434 size: 2413
```

The images are now published.

> Note that the `digest: sha256:xxx` hashes reported when pushing are not image
> IDs. After pushing, running `docker image inspect $IMAGE` will show the listed
> digests under the image's `RepoDigests` property.
