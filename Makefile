# This Makefile follows the advice in: https://tech.davis-hansson.com/p/make/

SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -O inherit_errexit -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
> $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

GENERATED_FILES = Dockerfile build-dev-image.sh build-release-image.sh push-release-image.sh pre-version.sh test-dev-images.bats test-release-images.bats
GENERATED_FILES_JOINED = $(shell echo "$(GENERATED_FILES)" | sed -e 's/ /,/g')
GENERATED_SCRIPTS = build-dev-image.sh build-release-image.sh push-release-image.sh pre-version.sh test-dev-images.bats test-release-images.bats
PROJECT_LOG = project-events.json-seq

help:
> echo "Available targets:"
> printf '  %s\n' $$(ggrep -P --only-matching '^[\w-]+(?=:)' Makefile)
.PHONY: help
.SILENT: help

.out:
> mkdir -p .out

clean:
> rm -rf .out
.PHONY: clean

clean-generated-files:
> chmod --silent +w $(GENERATED_FILES) || true
> rm .out/render-templates.sentinel $(GENERATED_FILES)
.PHONY: clean-generated-files

lint:
> deno lint
.PHONY: lint

check-format:
> deno fmt --check
.PHONY: check-format

auto-format-files:
> deno fmt
.PHONY: auto-format-files

render-templates: .out/render-templates.sentinel
.PHONY: render-templates

.out/render-templates.sentinel: .out $(shell find lib templates render-templates.ts Makefile config.json -type f)
> chmod --silent +w $(GENERATED_FILES) || true
> deno run --allow-read=. \
>   --allow-write=$(GENERATED_FILES_JOINED) \
>   render-templates.ts
> # make generated files unwritable to prevent accidental manual changes
> chmod -w $(GENERATED_FILES)
> chmod +x $(GENERATED_SCRIPTS)
> touch $@
.SILENT: .out/render-templates.sentinel

build-dev-image: .out/dev-image-build-events.json-seq
> jq --seq < $<
.PHONY: build-dev-image
.SILENT: build-dev-image

.out/dev-image-build-events.json-seq: .out Dockerfile build-dev-image.sh
> ./build-dev-image.sh > $@
.SILENT: .out/dev-image-build-events.json-seq

test-dev-image: .out/dev-image-build-events.json-seq
> bats --jobs 8 ./test-dev-images.bats
.PHONY: test-dev-image

build-release-image: .out/release-image-build-events.json-seq
> source lib/eventlog.sh
> RELEASE_TAG="$$(git describe --abbrev=0 --match 'v[0-9]*')" || (echo "Error: $@: git tag for release not found" 1>&2; exit 1)
> EXISTING_RELEASE_BUILD="$$(eventlog_Read | eventlog_ReleaseImagesBuilt "$${RELEASE_TAG:?}" | eventlog_Last)"
> if [[ "$$EXISTING_RELEASE_BUILD" != "" ]]; then
>   echo "$@: An event for $${RELEASE_TAG:?} has already been committed to $(PROJECT_LOG):" 1>&2
>   <<<"$${EXISTING_RELEASE_BUILD}" jq --seq 1>&2
>   exit
> fi
> RELEASE_VERSION="$$(jq < config.json --raw-output .version)"
> if [[ "v$$RELEASE_VERSION" != $$RELEASE_TAG ]]; then
>   echo "$@: tag does not match version in config.json" \
      "(tag: $${RELEASE_TAG:?}, version: $${RELEASE_VERSION})" 1>&2
> fi
> echo "$@: appended events to $(PROJECT_LOG):" 1>&2
> < $< tee --append $(PROJECT_LOG) | jq --seq
> git add $(PROJECT_LOG)
> git commit -m \
    "chore(release): log event ReleaseImagesBuilt for $${RELEASE_VERSION:?}" \
    -- $(PROJECT_LOG)
.PHONY: build-release-image
.SILENT: build-release-image

.out/release-image-build-events.json-seq: .out Dockerfile build-release-image.sh
> ./build-release-image.sh > $@
.SILENT: .out/release-image-build-events.json-seq

test-release-image:
> bats --jobs 8 ./test-release-images.bats
.PHONY: test-release-image

push-release-image: .out/push-release-image.sentinel
.PHONY: push-release-image

.out/push-release-image.sentinel: .out push-release-image.sh
> ./push-release-image.sh
> touch $@
.SILENT: .out/push-release-image.sentinel

bump-version:
> npx standard-version@^9.3.2 --commit-all
.PHONY: bump-version
