import { assert, eta } from "./deps.ts";
type Config = typeof eta.config;

/**
 * eta.renderFile() but always returns a promise, and disallows mixing data
 * with eta config.
 */
export async function renderFile(
  filename: string,
  data: Record<string, unknown>,
  config?: Partial<Config>,
): Promise<string> {
  // we must pass an object to config, otherwise eta interprets keys from data
  // as config options, and that means you can break eta rendering by adding
  // data that happens to use eta config options, e.g. 'tags' (which overrides
  // eta tags...)
  config = config || {};
  const result = eta.renderFile(filename, data, config);
  assert(typeof result === "object");
  return await result;
}
