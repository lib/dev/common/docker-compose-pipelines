import { path } from "./deps.ts";

const __dirname = path.dirname(path.fromFileUrl(import.meta.url));
const CONFIG_FILE = path.resolve(__dirname, "../config.json");

/**
 * The type of this repo's config file.
 */
export type Config = {
  version: string;
  dependencies: {
    docker: {
      imageTag: string;
      versionConstraint: string;
    };
    "docker-compose": {
      releaseUrl: string;
      versionConstraint: string;
    };
  };
  imageName: string;
  /**
   * The alternate versions of the image to build.
   *
   * Keys are a value to append to the image tag, values are targets in the
   * Dockerfile.
   */
  imageVariants: ReadonlyArray<
    { tagSuffix: string | null; target: string | null }
  >;
} & Record<string, unknown>;

export function isConfig(obj: unknown): obj is Config {
  if (!obj || typeof obj !== "object") return false;
  const config = obj as Partial<Config>;
  const deps = config.dependencies;
  return !!deps && typeof deps === "object" &&
    !!deps.docker && typeof deps.docker === "object" &&
    typeof deps.docker.imageTag === "string" &&
    typeof deps.docker.versionConstraint === "string" &&
    !!deps["docker-compose"] && typeof deps["docker-compose"] === "object" &&
    typeof deps["docker-compose"].releaseUrl === "string" &&
    typeof deps["docker-compose"].versionConstraint === "string" &&
    typeof config.imageName === "string" &&
    typeof config.version === "string" &&
    !!config.imageVariants && typeof config.imageVariants === "object" &&
    Array.isArray(config.imageVariants) &&
    config.imageVariants.every(({ tagSuffix, target }) =>
      (tagSuffix === null || typeof tagSuffix === "string") &&
        target === null || typeof target === "string"
    );
}

export async function readConfig(): Promise<Config> {
  const config = JSON.parse(await Deno.readTextFile(CONFIG_FILE));
  if (isConfig(config)) return config;
  throw new Error(`config.json does not have the expected structure`);
}
