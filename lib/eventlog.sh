# Bash functions for reading data from the JSON event log stream

# If stdin is not empty, pipe it through a command, but don't invoke the command
# if stdin is empty.
_filterNonEmptyStdin() {
  if [[ $# == 0 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi
  first_byte="$(head -c 1)"
  if [[ $first_byte != "" ]]; then
    {
      echo -n "$first_byte"
      cat
    } | "$@"
  fi
}

_jq_seq() {
  # jq --seq prints warnings or errors and can emit JSON null if it receives no
  # input. So only invoke jq if we actually have input.
  _filterNonEmptyStdin jq --seq "$@"
}

# Arguments: [$eventLog]
eventlog_Read() {
  if [[ $# > 1 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi
  if [[ $# == 1 ]]; then
    local eventLog="$1"
  else
    local eventLog="project-events.json-seq"
  fi
  _jq_seq < "${eventLog:?}"
}

# Arguments: $gitReleaseTag
eventlog_ReleaseImagesBuilt() {
  if [[ $# != 1 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi
  local gitReleaseTag="$1"

  _jq_seq --null-input --arg releaseTag "${gitReleaseTag:?}" '
    inputs | select(.event == "ReleaseImagesBuilt"
                    and .releaseGitTag == $releaseTag)'
}

eventlog_EventsOfType() {
  if [[ $# != 1 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi

  _jq_seq --null-input --arg eventType "${1:?}" '
    inputs | select(.event == $eventType)'
}

eventlog_Last() {
  if [[ $# != 0 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi

  _jq_seq --null-input 'last(inputs)'
}

eventlog_ImageIdsByRepoTag() {
  if [[ $# != 0 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi
  _jq_seq '
    [ .images[] | . as $image | .repoTags[] | {key: ., value: $image.id} ]
    | from_entries'
}

eventlog_ImageIdByRepoTag() {
  if [[ $# != 1 ]]; then
    echo "${FUNCNAME[0]}: invalid arguments" 1>&2; exit 1
  fi
  local repoTag="$1"
  _jq_seq --raw-output --arg repoTag "${repoTag:?}" '
    .[$repoTag] // null
    | if . == null then ("repoTag not found: \($repoTag)" | halt_error(1))
      else . end'
}
