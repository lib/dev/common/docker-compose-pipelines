import { assert, eta, path } from "./lib/deps.ts";
import { Config, readConfig } from "./lib/config.ts";
import { renderFile } from "./lib/templates.ts";

const BUILD_RELEASE_SCRIPT = "build-release-image.sh";
const PUSH_RELEASE_SCRIPT = "push-release-image.sh";
const TEST_RELEASE_SCRIPT = "test-release-images.bats";

type ImageCollectionBuildSpec =
  | DevImageCollectionBuildSpec
  | ReleaseImageCollectionBuildSpec;

type DevImageCollectionBuildSpec = {
  buildType: "dev";
} & BaseImageCollectionBuildSpec;
type ReleaseImageCollectionBuildSpec = {
  buildType: "release";
  releaseGitTag: string;
} & Required<BaseImageCollectionBuildSpec>;
type BaseImageCollectionBuildSpec = {
  imageBuildSpecs: ImageBuildSpec[];
};

type ImageLabel<Name extends string = string, Value extends string = string> =
  `${Name}=${Value}`;
type ImageBuildSpec = {
  /** The --target to build. */
  target: string | null;
  /** The full "<repo>/<name>:<tag>" values for the built image. */
  repoTags: ReadonlyArray<string>;
  /** labels to attach to the image (--label) */
  labels?: ReadonlyArray<ImageLabel>;
};

interface ImageVariant {
  tagSuffix: string | null;
  target: string | null;
}

function generateVersionTags(version: string): string[] {
  const match = /^(\d+)\.(\d+)\.(\d+)$/.exec(version);
  if (!match) {
    throw new Error(`unexpected version value: ${version}`);
  }
  const [major, minor, patch] = match.slice(1);
  return [
    `${major}`,
    `${major}.${minor}`,
    `${major}.${minor}.${patch}`,
  ];
}

function generateImageBuildSpecs({ imageName, tags, variants }: {
  imageName: string;
  tags: ReadonlyArray<string>;
  variants: ReadonlyArray<ImageVariant>;
}): Pick<ImageBuildSpec, "target" | "repoTags">[] {
  if (
    new Set(variants.map(({ tagSuffix }) => tagSuffix)).size < variants.length
  ) {
    throw new Error(`Duplicate tagSuffix`);
  }
  if (variants.length < 1) {
    throw new Error("at least one image variant is required");
  }
  if (tags.length < 1) throw new Error("at least one image tag is required");

  return variants.map(({ tagSuffix, target }) => {
    const suffix = tagSuffix ? `-${tagSuffix}` : "";
    const fullTags = tags.map((tag) => `${tag}${suffix}`);
    return {
      target,
      repoTags: fullTags.map((tag) => `${imageName}:${tag}`),
    };
  });
}

function releaseBuildSpecs(
  { config, releaseGitTag }: { config: Config; releaseGitTag: string },
): ImageBuildSpec[] {
  return generateImageBuildSpecs({
    imageName: config.imageName,
    tags: generateVersionTags(config.version),
    variants: config.imageVariants,
  }).map((spec) => ({
    ...spec,
    labels: [
      `org.opencontainers.image.version=${releaseGitTag}`,
      "org.opencontainers.image.revision=${COMMIT_FULL_HASH:?}",
    ],
  }));
}

type ReleaseTemplateContext =
  & typeof DEFAULT_TEMPLATE_CONTEXT
  & ImageCollectionBuildSpec
  & { mainScriptFilename: string };

async function generateReleaseImageFiles(config: Config): Promise<void> {
  const releaseGitTag = `v${config.version}`;
  const context: typeof DEFAULT_TEMPLATE_CONTEXT & ImageCollectionBuildSpec = {
    ...DEFAULT_TEMPLATE_CONTEXT,

    buildType: "release",
    imageBuildSpecs: releaseBuildSpecs({ config, releaseGitTag }),
    releaseGitTag,
  };

  await Promise.all([
    (async () =>
      Deno.writeTextFile(
        BUILD_RELEASE_SCRIPT,
        await generateBuildReleaseScript({
          ...context,
          mainScriptFilename: BUILD_RELEASE_SCRIPT,
        }),
      ))(),
    (async () =>
      Deno.writeTextFile(
        PUSH_RELEASE_SCRIPT,
        await generatePushReleaseScript({
          ...context,
          mainScriptFilename: PUSH_RELEASE_SCRIPT,
        }),
      ))(),
    (async () =>
      Deno.writeTextFile(
        TEST_RELEASE_SCRIPT,
        await generateReleaseTests({
          ...context,
          mainScriptFilename: TEST_RELEASE_SCRIPT,
        }),
      ))(),
  ]);
}

async function generateBuildReleaseScript(
  context: ReleaseTemplateContext,
): Promise<string> {
  return await renderFile("build-release-image.sh.eta", context);
}

async function generatePushReleaseScript(
  context: ReleaseTemplateContext,
): Promise<string> {
  return await renderFile("push-release-image.sh.eta", context);
}

async function generateReleaseTests(
  context: ReleaseTemplateContext,
): Promise<string> {
  return await renderFile("test-images.bats.eta", context);
}

type DevTemplateContext =
  & typeof DEFAULT_TEMPLATE_CONTEXT
  & Required<ImageCollectionBuildSpec>;

async function generateDevImageFiles(config: Config): Promise<void> {
  const context: DevTemplateContext = {
    ...DEFAULT_TEMPLATE_CONTEXT,

    buildType: "dev",
    imageBuildSpecs: generateImageBuildSpecs({
      imageName: config.imageName,
      tags: ["dev"],
      variants: config.imageVariants,
    }),
  };

  await Promise.all([
    (async () =>
      Deno.writeTextFile(
        "build-dev-image.sh",
        await generateDevBuildScript(context),
      ))(),
    (async () =>
      Deno.writeTextFile(
        "test-dev-images.bats",
        await generateDevTests(context),
      ))(),
  ]);
}

async function generateDevBuildScript(
  context: DevTemplateContext,
): Promise<string> {
  return await renderFile("build-dev-image.sh.eta", context);
}

async function generateDevTests(context: DevTemplateContext): Promise<string> {
  return await renderFile("test-images.bats.eta", context);
}

const DEFAULT_TEMPLATE_CONTEXT = {
  fn: {
    assert: assert,
    dirname: path.dirname,
  } as const,
} as const;

if (import.meta.main) {
  eta.configure({ views: path.join(Deno.cwd(), "templates") });
  const config = await readConfig();

  await Promise.all([
    (async () =>
      Deno.writeTextFile(
        "Dockerfile",
        (await renderFile("Dockerfile.eta", {
          ...DEFAULT_TEMPLATE_CONTEXT,
          config,
        })),
      ))(),

    generateReleaseImageFiles(config),

    generateDevImageFiles(config),

    (async () =>
      Deno.writeTextFile(
        "pre-version.sh",
        (await renderFile("pre-version.sh.eta", {
          ...DEFAULT_TEMPLATE_CONTEXT,
        })),
      ))(),
  ]);
}
