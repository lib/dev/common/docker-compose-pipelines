#!/usr/bin/env bash
###################################################
# Generated by render-templates.ts, DO NOT MODIFY #
###################################################

set -e -u -o pipefail
shopt -s inherit_errexit

DIRTY_FILES="$(git status --porcelain)"
if [ "$DIRTY_FILES" != "" ]; then
  echo "Error: git repo has uncommitted changes, refusing to build as the result may not be reproducible" 1>&2
  echo "$DIRTY_FILES" 1>&2
  exit 1
fi
